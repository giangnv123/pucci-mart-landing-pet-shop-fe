"use strict";

const gBASE_URL_PETS = "https://pucci-mart.onrender.com/api/pets";
const allPetsContainer = $(".all-pets-container");

const perPage = 8; // Number of pets to display per page
var vAllPetsArr = []; // Array to store all pets
var vTotalPage = 0;
var vCurrentPage = 1;

$(() => {
  initial();

  // Handle Event Listen Page Change
  listenPageChanges();

  // Event listener for all item number button
  function listenPageChanges() {
    $(".numbers-container").on("click", ".number-btn", function () {
      vCurrentPage = parseInt($(this).text());
      displayPagePet();
    });

    // Event listener for previous button and next button
    $(".pre-btn").click(() => {
      if (vCurrentPage > 1) {
        vCurrentPage--;
        displayPagePet();
      }
    });

    $(".next-btn").click(() => {
      if (vCurrentPage < vTotalPage) {
        vCurrentPage++;
        displayPagePet();
      }
    });
  }
});

// Function to fetch all pets from the API
async function getAllPets() {
  try {
    var vResponse = await fetch(gBASE_URL_PETS);
    var vData = await vResponse.json();
    var vAllPets = vData.rows;
    return vAllPets || [];
  } catch (error) {
    console.log(error.message);
    return [];
  }
}

// Function to store all pets in vAllPetsArr array
async function storeAllPets() {
  var vAllPets = await getAllPets();

  vAllPets.forEach((pet) => {
    var vHtml = `<div class="card shadow">
                    <img
                        src="${pet.imageUrl}"
                        alt="${pet.name}"
                        class="card-img-top "
                    />
                    <div class="card-body">
                    <h4>${pet.name}</h4>
                    <p>${pet.description}</p>
                    <div class="price-box">
                        <span class="sale-price">$${pet.promotionPrice}</span>
                        <span class="real-price">$${pet.price}</span>
                    </div>
                    </div>
                </div>`;
    vAllPetsArr.push(vHtml);
  });
  vTotalPage = Math.ceil(vAllPetsArr.length / perPage);
}

// Display the first page of pets
async function displayPagePet() {
  allPetsContainer.empty();
  var vAllPetsHtml = vAllPetsArr
    .slice((vCurrentPage - 1) * perPage, vCurrentPage * perPage)
    .map((pet) => pet)
    .join("");

  allPetsContainer.html(vAllPetsHtml);
  updateNumberListUI();
}

// Function to update the number buttons for pagination
function updateNumberListUI() {
  $(".numbers-container").empty();

  let vStart = 1;
  let vEnd = vTotalPage;

  if (vTotalPage > 3) {
    vCurrentPage === 1
      ? (vStart = vCurrentPage)
      : vCurrentPage === vTotalPage
      ? (vStart = vCurrentPage - 2)
      : (vStart = vCurrentPage - 1);
    vEnd = vStart + 2;
  }

  var vFragment = document.createDocumentFragment();
  for (let i = vStart; i <= vEnd; i++) {
    var vBtn = createButton(i, i == vCurrentPage);
    vFragment.append(vBtn[0]);
  }
  $(".numbers-container").append(vFragment);

  // Update btn pre and next
  vCurrentPage === 1
    ? $(".pre-btn").addClass("disabled")
    : $(".pre-btn").removeClass("disabled");

  vCurrentPage === vTotalPage
    ? $(".next-btn").addClass("disabled")
    : $(".next-btn").removeClass("disabled");
}

// Function to initialize the page
async function initial() {
  await storeAllPets();
  displayPagePet();
}

// Helper function to create a number button
function createButton(page, isActive) {
  return $("<button>")
    .addClass(`number-btn ${isActive && "active"}`)
    .text(page);
}

// Nav Toggle
$(".nav-toggle").click(function () {
  $(this).toggleClass("show-toggle");
  $(".nav-content").toggleClass(`show-nav`);
});
